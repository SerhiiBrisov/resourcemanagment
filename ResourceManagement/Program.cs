﻿using ResourceManagement.UnmanadgedResources;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace ResourceManagement
{
    class Program
    {
        private static bool UsePrintToConsole = false;

        private static void ViewInHex(object fileName)
        {
            byte[] bytes;
            using (FileManager reader = new FileManager((string)fileName))
                bytes = reader.ReadContents(20);

            if (Program.UsePrintToConsole)
            {
                //
                // Print up to 20 bytes.
                //
                int printNBytes = Math.Min(20, bytes.Length);
                Console.WriteLine("First {0} bytes of {1} in hex", printNBytes, fileName);
                for (int i = 0; i < printNBytes; i++)
                    Console.Write("{0:x} ", bytes[i]);
                Console.WriteLine();
                Program.UsePrintToConsole = false;
            }
        }

        static void Main(string[] args)
        {
            string fileName = "File.txt";
            int numIterations = 0;
            while (true)
            {
                Program.ViewInHex(fileName);
                numIterations++;
                if (numIterations % 100000 == 0)
                    Program.UsePrintToConsole = true;
            }
        }
    }
}