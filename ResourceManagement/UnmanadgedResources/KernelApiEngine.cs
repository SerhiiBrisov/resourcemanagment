﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Security;
using System.Runtime.ConstrainedExecution;

namespace ResourceManagement.UnmanadgedResources
{
    /// <summary>
    /// Class representing logic calling external services from kernel32 api.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    public class KernelApiEngine
    {
        /// <summary>
        /// Closes the handle.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <returns></returns>
        /// <owner>Serhii Brysov</owner>
        public static bool CloseHandle(IntPtr handle) => NativeMethods.CloseHandle(handle);

        /// <summary>
        /// Wrapper for native method that creates the file.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="fileAccess">The file access.</param>
        /// <param name="fileShare">The file share.</param>
        /// <param name="securityAttributes">The security attributes.</param>
        /// <param name="creationDisposition">The creation disposition.</param>
        /// <param name="flags">The flags.</param>
        /// <param name="template">The template.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">dataToWrite parameter cannot be null!</exception>
        public SafeFileHandleEx CreateFile(
            string fileName,
            FileAccess fileAccess = FileAccess.ReadWrite,
            FileShare fileShare = FileShare.ReadWrite | FileShare.Delete,
            IntPtr securityAttributes = default,
            FileMode creationDisposition = FileMode.Open,
            int flags = (int)FileAttributes.Normal,
            IntPtr template = default)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new ArgumentNullException($"{nameof(fileName)} cannot be empty.");

            return NativeMethods.CreateFile(fileName, fileAccess, fileShare, securityAttributes, creationDisposition, flags, template);
        }

        /// <summary>
        /// Reads the file.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <param name="bytes">The bytes.</param>
        /// <param name="numBytesToRead">The number bytes to read.</param>
        /// <param name="numBytesRead">The number bytes read.</param>
        /// <param name="overlappedPos">The overlapped position.</param>
        /// <returns></returns>
        /// <owner>Serhii Brysov</owner>
        public int ReadFile(SafeFileHandleEx handle, byte[] bytes, int numBytesToRead, out int numBytesRead, IntPtr overlappedPos = default) =>
            NativeMethods.ReadFile(handle, bytes, numBytesToRead, out numBytesRead, overlappedPos);

        /// <summary>
        /// Class representing P/Invoke methods.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        [SuppressUnmanagedCodeSecurity()]
        private static class NativeMethods
        {
            /// <summary>
            /// Free the kernel's file object (closes the handle).
            /// </summary>
            /// <owner>Serhii Brysov</owner>
            /// <param name="handle">The handle.</param>
            /// <returns>true if file closed; otherwise false</returns>
            [DllImport("kernel32", SetLastError = true)]
            [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
            public extern static bool CloseHandle(IntPtr handle);

            /// <summary>
            /// Allocates a file object in the kernel, then return a handle to it.
            /// </summary>
            /// <owner>Serhii Brysov</owner>
            /// <param name="fileName">Name of the file.</param>
            /// <param name="fileAccess">The file access.</param>
            /// <param name="fileShare">The file share.</param>
            /// <param name="securityAttributes">The security attributes.</param>
            /// <param name="creationDisposition">The creation disposition.</param>
            /// <param name="flags">The flags.</param>
            /// <param name="template">The template.</param>
            /// <returns>Tha handle to file.</returns>
            [DllImport("kernel32", SetLastError = true, CharSet = CharSet.Unicode)]
            public extern static SafeFileHandleEx CreateFile(String fileName,
               FileAccess fileAccess, FileShare fileShare,
               IntPtr securityAttributes, FileMode creationDisposition,
               int flags, IntPtr template);

            /// <summary>
            /// Reads the file.
            /// </summary>
            /// <owner>Serhii Brysov</owner>
            /// <param name="handle">The handle.</param>
            /// <param name="bytes">The bytes.</param>
            /// <param name="numBytesToRead">The number bytes to read.</param>
            /// <param name="numBytesRead">The number bytes read.</param>
            /// <param name="overlapped_MustBeZero">The overlapped must be zero.</param>
            /// <returns></returns>
            [DllImport("kernel32", SetLastError = true)]
            public extern static int ReadFile(SafeFileHandleEx handle, byte[] bytes,
               int quantityBytesToRead, out int quantityBytesRead, IntPtr overlappedPos);
        }
    }
}
