﻿using System;
using System.Runtime.InteropServices;
using System.IO;
using System.ComponentModel;
using System.Security.Permissions;

namespace ResourceManagement.UnmanadgedResources
{
    /// <summary>
    /// Class providing logic to work with file.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    /// <seealso cref="System.IDisposable" />
    public class FileManager : IDisposable
    {
        /// <summary>
        /// Is the value disposed.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private bool disposedValue;

        /// <summary>
        /// The kernel API.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private KernelApiEngine kernelApi = new KernelApiEngine();

        /// <summary>
        /// The file handle.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private readonly SafeFileHandleEx fileHandle;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileManager"/> class.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="fileName">Name of the file.</param>
        /// <exception cref="Win32Exception">Erorr occured accesing the file.</exception>
        public FileManager(string fileName)
        {
            //
            // Security permission check.
            //
            string fullPath = Path.GetFullPath(fileName);
            new FileIOPermission(FileIOPermissionAccess.Read, fullPath).Demand();

            //
            // Creating of safehandle.
            //
            this.fileHandle = this.kernelApi.CreateFile(fileName, FileAccess.Read, FileShare.Read, IntPtr.Zero, FileMode.Open, 0, IntPtr.Zero);

            if (this.fileHandle.IsInvalid)
                throw new Win32Exception(Marshal.GetLastWin32Error(), fileName);
        }

        /// <summary>
        /// Reads the contents.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="length">The length.</param>
        /// <returns>The bytes had been read.</returns>
        /// <exception cref="ObjectDisposedException">FileReader is closed</exception>
        /// <exception cref="Win32Exception"></exception>
        [SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
        public byte[] ReadContents(int length)
        {
            if (this.fileHandle.IsInvalid)
                throw new ObjectDisposedException("FileReader is closed");

            byte[] bytes = new byte[length];
            int result = this.kernelApi.ReadFile(this.fileHandle, bytes, length, out int numRead, IntPtr.Zero);

            if (result == 0)
                throw new Win32Exception(Marshal.GetLastWin32Error());

            if (numRead < length)
            {
                byte[] newBytes = new byte[numRead];
                Array.Copy(bytes, newBytes, numRead);
                bytes = newBytes;
            }
            return bytes;
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        [SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
        protected virtual void Dispose(bool disposing)
        {            
            if (!this.disposedValue && !this.fileHandle.IsInvalid)
            {
                if (disposing)
                {
                    //
                    // Free managed wrapper.
                    //
                    this.fileHandle.Dispose();
                }

                //
                // Free unmanaged resource.
                //
                this.kernelApi = null;

                this.disposedValue = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        public void Dispose()
        {
            this.Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        //~FileManager()
        //{
        //    this.Dispose(false);
        //}
    }
}
