﻿using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using System.Runtime.ConstrainedExecution;

namespace ResourceManagement.UnmanadgedResources
{
    /// <summary>
    /// Represents implementation of SafeHandle.
    /// </summary>
    /// <owner>Serhii Brysov</owner>
    /// <seealso cref="Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid" />
    [SecurityPermission(SecurityAction.InheritanceDemand, UnmanagedCode = true)]
    [SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
    public class SafeFileHandleEx : SafeHandleZeroOrMinusOneIsInvalid
    {
        /// <summary>
        /// Makes the handle free.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        /// <returns>
        ///   <see langword="true" /> if the handle is released successfully; otherwise, in the event of a catastrophic failure,
        ///   <see langword="false" />. In this case, it generates a releaseHandleFailed Managed Debugging Assistant.
        /// </returns>
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
        protected override bool ReleaseHandle() => KernelApiEngine.CloseHandle(this.handle);

        /// <summary>
        /// Create a SafeHandle, informing the base class
        /// that this SafeHandle instance "owns" the handle,
        /// and therefore SafeHandle should call
        /// our ReleaseHandle method when the SafeHandle is no longer in use.
        /// </summary>
        /// <owner>Serhii Brysov</owner>
        private SafeFileHandleEx()
            : base(true)
        {
        }
    }
}
